#ifndef VECTOR_HPP
#define VECTOR_HPP


namespace aux {

struct empty_vector {};

struct null_type {};

template<typename Head, class Tail>
struct vector {
  typedef Head H;
  typedef Tail T;
};

} // namespace aux


#define VECTOR_0 aux::empty_vector
#define VECTOR_1(T) aux::vector<T, aux::null_type>
#define VECTOR_2(T1, T2) aux::vector<T1, VECTOR_1(T2)>
#define VECTOR_3(T1, T2, T3) aux::vector<T1, VECTOR_2(T2, T3)>
#define VECTOR_4(T1, T2, T3, T4) aux::vector<T1, VECTOR_3(T2, T3, T4)>
#define VECTOR_5(T1, T2, T3, T4, T5) aux::vector<T1, VECTOR_4(T2, T3, T4, T5)>


#endif // VECTOR_HPP
