#ifndef UTILS_IS_SAME_HPP
#define UTILS_IS_SAME_HPP


template<typename T, typename U>
struct is_same {
  static const bool value = false;
};

template<typename T>
struct is_same<T, T> {
  static const bool value = true;
};


#endif // UTILS_IS_SAME_HPP
