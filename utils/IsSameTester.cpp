#include <gtest/gtest.h>

#include "is_same.hpp"

class IsSameTester : public testing::Test {};


TEST_F(IsSameTester, testIsSame) {
  ASSERT_TRUE((is_same<int, int>::value));
  ASSERT_FALSE((is_same<int, float>::value));
}


int main(int argc, char* argv[]) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
