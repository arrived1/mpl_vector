#clang++ -std=c++11 -L/home/przybyl/Programming/gtest-1.6.0/lib/.libs/ VectorTester.cpp -o VectorTester -I/home/przybyl/Programming/gtest-1.6.0/include -I/home/przybyl/Programming/mpl_vector -lgtest -pthread

GTESTPATH=/home/przybyl/Programming/gtest-1.6.0
PROJECTPATH=/home/przybyl/Programming/mpl_vector

CC=clang++
CFLAGS=-std=c++11

LFLAGS=-L$(GTESTPATH)/lib/.libs/
INCLUDE=-I$(GTESTPATH)/include -I$(PROJECTPATH) 
LIBS=-lgtest -pthread

TESTOUT=VectorTester

test: VectorTester.cpp
	$(CC) $(CFLAGS) $(LFLAGS) VectorTester.cpp -o $(TESTOUT) $(INCLUDE) $(LIBS)

clean:
	rm  $(TESTOUT) 
