#ifndef VECTOR_FRONT_HPP
#define VECTOR_FRONT_HPP

#include "vector.hpp"


template<class T>
struct front;

template<typename H, class T>
struct front<aux::vector<H, T>> {
	typedef H type;
};

#endif // VECTOR_FRONT_HPP
