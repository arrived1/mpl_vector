#ifndef VECTOR_PUSH_FRONT_HPP
#define VECTOR_PUSH_FRONT_HPP

#include "vector.hpp"


template<class vec, typename H>
struct push_front {
	typedef typename aux::vector<H, vec> type;
};


template<typename H>
struct push_front<aux::empty_vector, H> {
	typedef typename aux::vector<H, aux::null_type> type;
};

#endif // VECTOR_PUSH_FRONT_HPP
