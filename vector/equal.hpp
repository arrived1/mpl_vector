#ifndef VECTOR_EQUAL_HPP
#define VECTOR_EQUAL_HPP

#include "vector.hpp"


template<class Vec1, class Vec2>
struct equal {
  static const bool value = false;
};


template<>
struct equal<aux::empty_vector, aux::empty_vector> {
  static const bool value = true;
};

template<>
struct equal<aux::null_type, aux::null_type> {
  static const bool value = true;
};

template<typename H, class T1, class T2>
struct equal<aux::vector<H, T1>, aux::vector<H, T2>> {
  static const bool value = equal<T1, T2>::value;
};


#endif // VECTOR_EQUAL_HPP
