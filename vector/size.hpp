#ifndef VECTOR_SIZE_HPP
#define VECTOR_SIZE_HPP

#include "vector.hpp"


template<class Vector>
struct size {
  static const int value;
};

template<> const int size<aux::empty_vector>::value = 0;


template<> const int size<aux::null_type>::value = 0;

template<typename H, class T>
struct size<aux::vector<H, T>> {
  static const int value;
};

template<typename H, class T> const int size<aux::vector<H, T>>::value = 1 + size<T>::value;


#endif // VECTOR_SIZE_HPP
