#ifndef VECTOR_SORT_STATIC_IF_HPP
#define VECTOR_SORT_STATIC_IF_HPP

#include "vector.hpp"

template<bool, class B, class C>
struct static_if;

template<class A, class B>
struct static_if<true, A, B> {
	typedef A type;
};

template<class A, class B>
struct static_if<false, A, B> {
	typedef B type;
};

#endif // VECTOR_SORT_STATIC_IF_HPP