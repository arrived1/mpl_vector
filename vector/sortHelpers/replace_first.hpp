#ifndef VECTOR_SORT_REPLACE_FIRST_HPP
#define VECTOR_SORT_REPLACE_FIRST_HPP

#include "vector.hpp"

template<class Vec, typename From, typename To>
struct replace_first;

template<typename From, typename To>
struct replace_first<aux::empty_vector, From , To> {
	typedef aux::empty_vector type;
};
//TODO: test it
template<typename From, typename To>
struct replace_first<aux::null_type, From , To> {
	typedef aux::null_type type;
};

template<typename From, typename To, typename T>
struct replace_first<aux::vector<From, T>, From, To> {
	typedef aux::vector<To, T> type;
};

template<typename From, typename To, typename H, typename T>
struct replace_first<aux::vector<H, T>, From, To> {
	typedef aux::vector<H, typename replace_first<T, From, To>::type> type;
};

#endif // VECTOR_SORT_REPLACE_FIRST_HPP