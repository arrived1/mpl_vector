#ifndef VECTOR_SORT_LOWER_SIZE_HPP
#define VECTOR_SORT_LOWER_SIZE_HPP

#include "vector.hpp"

template<class A, class B>
struct lower_size;

template<class A, class B>
struct lower_size {
	typedef typename  static_if<sizeof(A) < sizeof(B), A, B>::type type;
};

#endif // VECTOR_SORT_LOWER_SIZE_HPP