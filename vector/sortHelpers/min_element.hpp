#ifndef VECTOR_SORT_MIN_ELEMENT_HPP
#define VECTOR_SORT_MIN_ELEMENT_HPP

#include "vector.hpp"

template<class H, template<class, class> class P>
struct min_element;

template<class H, template<class, class> class P>
struct min_element<aux::vector<H, aux::null_type>, P> {
	typedef H type;
};

template<class H, class T, template<class, class> class P>
struct min_element<aux::vector<H, T>, P> {
	typedef typename P<H, typename min_element<T, P>::type>::type type;
};


#endif // VECTOR_SORT_MIN_ELEMENT_HPP