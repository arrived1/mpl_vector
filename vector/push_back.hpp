#ifndef VECTOR_PUSH_BACK_HPP
#define VECTOR_PUSH_BACK_HPP

#include "vector.hpp"


template<class vec, typename U>
struct push_back;

template<typename U>
struct push_back<aux::empty_vector, U> {
	typedef VECTOR_1(U) type;
};

template<typename T, typename U>
struct push_back<aux::vector<T, aux::null_type>, U> {
	typedef VECTOR_2(T, U) type;
};

template<typename H, typename T, typename U>
struct push_back<aux::vector<H, T>, U> {
	typedef typename aux::vector<H, typename push_back<T, U>::type> type;
};

#endif // VECTOR_PUSH_BACK_HPP
