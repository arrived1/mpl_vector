#ifndef VECTOR_BACK_HPP
#define VECTOR_BACK_HPP

#include "vector.hpp"


template<class vec>
struct back;

template<typename H, class T>
struct back<aux::vector<H, T>> {
	typedef typename back<T>::type type;
};

template<typename H>
struct back<aux::vector<H, aux::null_type>> {
	typedef H type;
};


#endif // VECTOR_BACK_HPP
