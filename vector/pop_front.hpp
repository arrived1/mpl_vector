#ifndef VECTOR_POP_FRONT_HPP
#define VECTOR_POP_FRONT_HPP

#include "vector.hpp"


template<class vec>
struct pop_front; 

template<typename H>
struct pop_front<aux::vector<H, aux::null_type>> {
	typedef typename VECTOR_0 type;
};

template<typename H, typename T>
struct pop_front<aux::vector<H, T>> {
	typedef T type;
};


#endif // VECTOR_POP_FRONT_HPP
