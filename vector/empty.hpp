#ifndef VECTOR_EMPTY_HPP
#define VECTOR_EMPTY_HPP

#include "vector.hpp"


template<class vec>
struct empty {
	static const bool value = false;
};

template<>
struct empty<class aux::empty_vector> {
	static const bool value = true;
};


#endif // VECTOR_EMPTY_HPP