#ifndef VECTOR_CLEAR_HPP
#define VECTOR_CLEAR_HPP

#include "vector.hpp"


template<class vec>
struct clear {
	typedef typename aux::empty_vector type;
};

#endif // VECTOR_CLEAR_HPP