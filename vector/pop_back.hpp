#ifndef VECTOR_POP_BACK_HPP
#define VECTOR_POP_BACK_HPP

#include "vector.hpp"


template<class vec>
struct pop_back;

template<typename H>
struct pop_back<aux::vector<H, aux::null_type>> {
	typedef typename aux::empty_vector type;
};


template<typename H, typename T>
struct pop_back<aux::vector<H, aux::vector<T, aux::null_type>>> {
	typedef typename aux::vector<H, aux::null_type> type;
};

template<typename H, typename T>
struct pop_back<aux::vector<H, T>> {
	typedef typename aux::vector<H, typename pop_back<T>::type> type;
};

#endif // VECTOR_POP_BACK_HPP
