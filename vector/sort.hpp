#ifndef VECTOR_SORT_HPP
#define VECTOR_SORT_HPP

#include "vector.hpp"
#include "sortHelpers/static_if.hpp"
#include "sortHelpers/lower_size.hpp"
#include "sortHelpers/replace_first.hpp"
#include "sortHelpers/min_element.hpp"
#include "sortHelpers/bigger_size.hpp"


template<class vec, template<class, class> class Pred>
struct sort;

template<template<class, class> class Pred>
struct sort<aux::empty_vector, Pred> {
	typedef aux::empty_vector type;
};

template<template<class, class> class Pred>
struct sort<aux::null_type, Pred> {
	typedef aux::null_type type;
};


template<class H, class T, template<class, class> class Pred>
struct sort<aux::vector<H, T>, Pred> {
private:
	typedef typename min_element<aux::vector<H, T>, Pred>::type First;
	typedef typename replace_first<T, First, H>::type Tprim;
	typedef typename sort<Tprim, Pred>::type sortedT;


public:
	typedef aux::vector<First, sortedT> type;
};


#endif // VECTOR_SORT_HPP
