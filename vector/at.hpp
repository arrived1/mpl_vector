#ifndef VECTOR_AT_HPP
#define VECTOR_AT_HPP

#include "vector.hpp"


template<class Vector, unsigned i>
struct at;

template<typename H, class T>
struct at<aux::vector<H, T>, 0> {
  typedef H type;
};

template<typename H, class T, unsigned i>
struct at<aux::vector<H, T>, i> {
  typedef typename at<T, i - 1>::type type;
};


#endif // VECTOR_AT_HPP
