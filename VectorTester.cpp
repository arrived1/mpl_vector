#include <gtest/gtest.h>

#include "vector.hpp"
#include "vector/at.hpp"
#include "vector/equal.hpp"
#include "vector/push_back.hpp"
#include "vector/push_front.hpp"
#include "vector/size.hpp"
#include "vector/empty.hpp"
#include "vector/clear.hpp"
#include "vector/front.hpp"
#include "vector/back.hpp"
#include "vector/push_front.hpp"
#include "vector/push_back.hpp"
#include "vector/pop_front.hpp"
#include "vector/pop_back.hpp"
#include "vector/sort.hpp"

#include "utils/is_same.hpp"


class VectorTester : public testing::Test {};


TEST_F(VectorTester, testVectorDefinitions) {
  typedef VECTOR_0 v0;
  typedef VECTOR_1(int) v1;
  typedef VECTOR_2(int, float) v2;
  typedef VECTOR_3(int, float, char) v3;
  typedef VECTOR_4(int, float, char, bool) v4;
  typedef VECTOR_5(int, float, char, bool, int) v5;
}


TEST_F(VectorTester, testSizeEmptyVector) {
  ASSERT_EQ(0, size<VECTOR_0>::value);
}


TEST_F(VectorTester, testSizeNonEmptyVector) {
  typedef VECTOR_1(int) v1;
  ASSERT_EQ(1, size<v1>::value);
  
  typedef VECTOR_2(int, float) v2;
  ASSERT_EQ(2, size<v2>::value);
}


TEST_F(VectorTester, testAt) {
  typedef VECTOR_3(int, float, bool) vec;
  
  typedef at<vec, 0>::type at0;
  typedef at<vec, 2>::type at2;
  
  EXPECT_TRUE((is_same<at0, int>::value));
  EXPECT_TRUE((is_same<at2, bool>::value));
}


TEST_F(VectorTester, testEqualEmptyVector) {
  typedef VECTOR_0 v1;
  typedef VECTOR_0 v2;
  typedef VECTOR_1(int) v3;
  
  EXPECT_TRUE((equal<v1, v2>::value));
  EXPECT_FALSE((equal<v1, v3>::value));
}


TEST_F(VectorTester, testEqualNonEmptyVector) {
  typedef VECTOR_1(int) v1;
  typedef VECTOR_1(int) v2;
  typedef VECTOR_1(float) v3;
  typedef VECTOR_2(int, float) v4;
  
  EXPECT_TRUE((equal<v1, v2>::value));
  EXPECT_FALSE((equal<v1, v3>::value));
  EXPECT_FALSE((equal<v1, v4>::value));
}


TEST_F(VectorTester, testEqual) {
  typedef VECTOR_1(int) v1;
  typedef VECTOR_1(int) v2;

  EXPECT_TRUE((equal<v1, v2>::value));
}


TEST_F(VectorTester, testEmpty) {
  typedef VECTOR_0 v1;
  typedef VECTOR_1(int) v2;

  EXPECT_TRUE((empty<v1>::value));
  EXPECT_FALSE((empty<v2>::value));
}


TEST_F(VectorTester, testClear) {
  typedef VECTOR_0 empty;
  typedef VECTOR_0 v0;
  typedef VECTOR_1(int) v1;
  typedef VECTOR_2(int, float) v2;

  typedef clear<v0>::type clear0;
  typedef clear<v1>::type clear1;
  typedef clear<v2>::type clear2;

  EXPECT_TRUE((is_same<clear0, empty>::value));
  EXPECT_TRUE((is_same<clear1, empty>::value));
  EXPECT_TRUE((is_same<clear2, empty>::value));
}


TEST_F(VectorTester, testFront) {
  typedef VECTOR_2(bool, float) vec1;
  typedef VECTOR_3(int, float, bool) vec2;
  
  typedef front<vec1>::type front1;
  typedef front<vec2>::type front2;
  
  EXPECT_TRUE((is_same<front1, bool>::value));
  EXPECT_TRUE((is_same<front2, int>::value));
}


TEST_F(VectorTester, testBack) {
  typedef VECTOR_1(float) vec1;
  typedef VECTOR_3(int, float, bool) vec2;
  
  typedef back<vec1>::type back1;
  typedef back<vec2>::type back2;
  
  EXPECT_TRUE((is_same<back1, float>::value));
  EXPECT_TRUE((is_same<back2, bool>::value));
  EXPECT_FALSE((is_same<back2, int>::value));
}


TEST_F(VectorTester, testPushFront) {
  typedef VECTOR_0 v0;
  typedef VECTOR_1(int) v1;
  typedef VECTOR_2(float, int) v2;
  typedef VECTOR_3(bool, float, int) v3;

  typedef push_front<v0, int>::type pushFront0;
  typedef push_front<v1, float>::type pushFront1;
  typedef push_front<v2, bool>::type pushFront2;

  EXPECT_TRUE((is_same<pushFront0, v1>::value));
  EXPECT_TRUE((is_same<pushFront1, v2>::value));
  EXPECT_TRUE((is_same<pushFront2, v3>::value));
}


TEST_F(VectorTester, testPushBack_emptyVector) {
  typedef VECTOR_0 v0;
  typedef VECTOR_1(bool) v1;

  typedef push_back<v0, bool>::type pushBack0;
  EXPECT_TRUE((is_same<pushBack0, v1>::value));
};


TEST_F(VectorTester, testPushBack_2elements) {

  typedef VECTOR_1(bool) v1;
  typedef VECTOR_2(bool, int) v2;

  typedef push_back<v1, int>::type pushBack1;
  EXPECT_TRUE((is_same<pushBack1, v2>::value));
};


TEST_F(VectorTester, testPushBack_3elemens) {
  typedef VECTOR_2(bool, int) v2;
  typedef VECTOR_3(bool, int, double) v3;

  typedef push_back<v2, double>::type pushBack2;
  EXPECT_TRUE((is_same<pushBack2, v3>::value));
};


TEST_F(VectorTester, testpopFront_1element) {
  typedef VECTOR_0 v1;
  typedef VECTOR_1(int) v2;

  typedef pop_front<v2>::type popFront1;
  EXPECT_TRUE((equal<popFront1, v1>::value));
}


TEST_F(VectorTester, testpopFront_2elements) {
  typedef VECTOR_1(int) v1;
  typedef VECTOR_2(bool, int) v2;

  typedef pop_front<v2>::type popFront1;
  EXPECT_TRUE((equal<popFront1, v1>::value));
}


TEST_F(VectorTester, testPopBack_1element) {
  typedef VECTOR_0 v1;
  typedef VECTOR_1(int) v2;

  typedef pop_back<v2>::type popBack1;
  EXPECT_TRUE((equal<popBack1, v1>::value));
}


TEST_F(VectorTester, testpopBack_2elements) {
  typedef VECTOR_1(bool) v1;
  typedef VECTOR_2(bool, int) v2;

  typedef pop_back<v2>::type popBack1;
  EXPECT_TRUE((equal<popBack1, v1>::value));
}


TEST_F(VectorTester, testStaticIf) {
  EXPECT_TRUE((is_same<static_if<true, double, int>::type, double>::value));
  EXPECT_TRUE((is_same<static_if<false, double, int>::type, int>::value));
}


TEST_F(VectorTester, testLowerSize) {
  EXPECT_TRUE((is_same<lower_size<double, int>::type, int>::value));
}

TEST_F(VectorTester, replaceFirst_emptyVector) {
  typedef VECTOR_0 vec;
  typedef replace_first<vec, int, char>::type replace;

  EXPECT_TRUE((equal<replace, vec>::value));
}


TEST_F(VectorTester, replaceFirst) {
  typedef VECTOR_3(bool, int, int) vec;
  typedef VECTOR_3(bool, char, int) expectedVec;
  typedef replace_first<vec, int, char>::type replace;

  EXPECT_TRUE((equal<replace, expectedVec>::value));
}


TEST_F(VectorTester, testMinElemPair) {
  typedef VECTOR_2(double, int) vec;
  typedef min_element<vec, lower_size>::type min;

  EXPECT_TRUE((is_same<int, min>::value));
}


TEST_F(VectorTester, testMinElem) {
  typedef VECTOR_3(double, float, char) vec;
  typedef min_element<vec, lower_size>::type min;

  EXPECT_TRUE((is_same<char, min>::value));
}

TEST_F(VectorTester, testSort) {
  typedef VECTOR_0 empty;
  typedef VECTOR_4(int, char, double, char) vec;
  typedef VECTOR_4(char, char, int, double) res;
  typedef VECTOR_4(double, int, char, char) bigger;
  EXPECT_TRUE((is_same<sort<empty, lower_size>::type, empty>::value));
  EXPECT_TRUE((is_same<sort<vec, lower_size>::type, res>::value));
  EXPECT_TRUE((is_same<sort<vec, bigger_size>::type, bigger>::value));
}




int main(int argc, char* argv[]) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
